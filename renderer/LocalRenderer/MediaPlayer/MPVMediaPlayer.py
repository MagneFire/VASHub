from .MediaPlayer import MediaPlayer

import mpv

from threading import Event
from threading import Timer
from threading import Thread
import time

class MPVMediaPlayer(MediaPlayer):
    def log(self, loglevel, component, message):
        print('[{}] {}: {}'.format(loglevel, component, message))

    def __init__(self, callback):
        super().__init__(callback)
        self.player = mpv.MPV(log_handler=self.log, input_default_bindings=True, input_vo_keyboard=True)

        @self.player.property_observer('time-pos')
        def time_observer(_name, value):
            if value:
                self.callback(MediaPlayer.TIME, int(value))

        @self.player.property_observer('duration')
        def time_observer(_name, value):
            if value:
                self.callback(MediaPlayer.DURATION, int(value))

        @self.player.event_callback('end-file')
        def stop_event(event):
            self.callback(MediaPlayer.QUIT, 0)

        @self.player.on_key_press('q')
        def mpv_quit_key():
            self.callback(MediaPlayer.QUIT, 0)


    def play(self):
        self.player.play(self.media)

    def handle(self):
        return

    def pause(self):
        if self.player.pause:
            self.player.pause = False
        else:
            self.player.pause = True

    def seek(self, seconds):
        self.player.seek(seconds)

    def volume(self, volume):
        return

    def stop(self):
        self.player.quit()
        return
