from .MediaPlayer import MediaPlayer

import vlc
import sys
from threading import Event
from threading import Timer
from threading import Thread

class VLCMediaPlayer(MediaPlayer):
    def print_version(self):
        """Print version of this vlc.py and of the libvlc"""
        try:
            print('Build date: %s (%#x)' % (vlc.build_date, vlc.hex_version()))
            print('LibVLC version: %s (%#x)' % (vlc.libvlc_get_version().decode("utf-8"), vlc.libvlc_hex_version()))
            print('LibVLC compiler: %s' % vlc.libvlc_get_compiler().decode("utf-8"))
        except:
            print('Error: %s' % sys.exc_info()[1])

    def pos_callback(self, event, player):
        duration = int(player.get_length() / 1000)
        time = int(player.get_time() / 1000)
        if self.media_duration != duration:
            self.callback(MediaPlayer.DURATION, duration)
            self.media_duration = duration
        if self.media_time != time:
            self.callback(MediaPlayer.TIME, time)
            self.media_time = time

    def __init__(self, callback):
        super().__init__(callback)
        self.instance = vlc.Instance()
        self.player = None
        self.media_duration = 0
        self.media_time = 0
        self.print_version()

    def play(self):
        if not self.media:
            print("ERROR: No media file specified!")
            return

        self.player = self.instance.media_player_new()
        self.player.video_set_key_input(True)
        self.player.video_set_mouse_input(True)

        media = self.instance.media_new(self.media)
        self.player.set_media(media)
        self.player.play()
        self.player.set_fullscreen(True)
        event_manager = self.player.event_manager()
        event_manager.event_attach(vlc.EventType.MediaPlayerPositionChanged, self.pos_callback, self.player)

    def handle(self):
        return

    def pause(self):
        if self.player:
            if self.player.is_playing():
                self.player.pause()
            else:
                self.player.play()

    def seek(self, seconds):
        if self.player:
            self.player.set_time(self.player.get_time() + (seconds * 1000))

    def volume(self, volume):
        return

    def stop(self):
        if self.player:
            self.player.stop()
        return
