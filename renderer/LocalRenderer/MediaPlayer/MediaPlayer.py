# Interface file
class MediaPlayer():
    DURATION = 1
    TIME = 2
    QUIT = 3
    STATE = 4
    def __init__(self, callback):
        self.callback = callback
        self.title = None
        self.media = None
        self.thumb = None
        self.subtitles = None
        # Init
        return

    def play(self):
        return

    def handle(self):
        return

    def pause(self):
        return

    def seek(self, seconds):
        return

    def volume(self, volume):
        return

    def stop(self):
        return
