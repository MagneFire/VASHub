from ..Renderer import Renderer

try:
    from .MediaPlayer.VLCMediaPlayer import VLCMediaPlayer as MediaPlayer
except:
    try:
        from .MediaPlayer.MPVMediaPlayer import MPVMediaPlayer as MediaPlayer
    except:
        raise Exception("No valid media player can be used!")

class LocalRenderer(Renderer):
    def media_callback(self, key, value):
        if key == MediaPlayer.DURATION:
            self.callback(Renderer.DURATION, value)
        if key == MediaPlayer.TIME:
            self.callback(Renderer.TIME, value)
        if key == MediaPlayer.QUIT:
            self.callback(Renderer.QUIT, value)
        if key == MediaPlayer.STATE:
            self.callback(Renderer.STATE, value)
    def __init__(self, callback):
        super().__init__(callback)
        self.player = MediaPlayer(self.media_callback)

    def play(self, device, title, media_url, sub_lang, sub_url, thumb_url):
        self.player.media = media_url
        self.player.play()
        return

    def handle(self):
        self.player.handle()
        return

    def pause(self, pause=None):
        self.player.pause()
        return

    def seek(self, seconds, position=0):
        """ Absolute seek is unsupported. """
        self.player.seek(seconds)
        return

    def volume(self, volume):
        self.player.volume(volume)
        return

    def stop(self):
        self.player.stop()
        return
