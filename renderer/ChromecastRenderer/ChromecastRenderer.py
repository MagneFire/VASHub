from ..Renderer import Renderer

import select

import pychromecast
from threading import Event
from threading import Timer
from threading import Thread
import time

from .MediaController import LCController

class ChromecastRenderer(Renderer):
    class Timeline():
        def __init__(self, callback):
            self.callback = callback
            self.disabled = False
            # Setup thread for counting seconds.
            self.stop_counter = Event()
            self.offset = 0
            self.second_counter = 0
            self.thread = Thread(target=self.count_thread)
            self.thread.start()

        def count_thread(self):
            while not self.stop_counter.is_set():
                if not self.disabled:
                    self.second_counter += 1
                    self.current_time = self.offset + self.second_counter
                    self.callback(Renderer.TIME, self.current_time)
                self.stop_counter.wait(1)

        def reset(self, offset):
            self.offset = offset
            self.second_counter = 0
            self.disabled = False

        def disable(self):
            # Disables the counter.
            self.disabled = True

        def stop(self):
            # Stop chromecast thread.
            self.stop_counter.set()

    # Callback to receive in coming new chromecasts.
    def chromecast_callback(self, chromecast):
        print("Found chromecast: {}".format(chromecast.device.friendly_name))
        if not self.device or (chromecast.device.friendly_name == self.device):
            self.cast = chromecast
            # Signal main thread that the targeted chromecast is found.
            self.done_discovery.set()
            self.stop_discovery()

    def new_cast_status(self, status):
        self.namespaces = status.namespaces
        if self.media_volume != status.volume_level:
            self.media_volume = status.volume_level
            self.callback(Renderer.VOLUME, self.media_volume)
        if self.volume_muted != status.volume_muted:
            self.volume_muted = status.volume_muted
            self.callback(Renderer.VOLUME_MUTE, self.volume_muted)

    def new_media_status(self, status):
        self.lc.media_session_id = status.media_session_id
        if status.player_state:
            self.callback(Renderer.STATE, status.player_state)
        # Update and disable timeline counter.
        # This is done to update the timelinee, when another application changed the timeline(seek).
        # Disable immediately because the playback state can be a number of varying values.
        # The only state that should continue the timeline counter is playback state 'playing'.
        self.timeline.reset(int(status.current_time))
        self.timeline.disable()
        if status.player_state == "PLAYING":
            # Update the current time of the media.
            # And enable the timeline if it was disabled.
            self.timeline.reset(int(status.current_time))

        if status.duration:
            self.callback(Renderer.DURATION, int(status.duration))
        if status.current_time:
            # Immediately update the timeline, do not wait for the
            # thread to update the timeline after max. 1 second.
            self.callback(Renderer.TIME, int(status.current_time))

    def __init__(self, callback):
        super().__init__(callback)
        self.cast = None
        self.mc = None
        self.lc = LCController()
        self.device = None
        self.timeline = self.Timeline(callback)
        self.namespaces = []
        self.media_volume = 0

        self.load_subtitle_file = self.lc.load_subtitle_file
        self.load_subtitle_url = self.lc.load_subtitle_url
        self.load_subtitle = self.lc.load_subtitle

        self.set_subtitle_style = self.lc.set_subtitle_style

        self.toggle_subtitle = self.lc.toggle_subtitle
        self.hide_subtitle = self.lc.hide_subtitle
        self.show_subtitle = self.lc.show_subtitle
        self.subtitle_offset = self.lc.subtitle_offset
        self.subtitle_offset_reset = self.lc.subtitle_offset_reset
        self.image_rotate_left = self.lc.image_rotate_left
        self.image_rotate_right = self.lc.image_rotate_right
        self.image_zoom_in = self.lc.image_zoom_in
        self.image_zoom_out = self.lc.image_zoom_out
        self.set_speed = self.lc.set_speed

    def play(self, device, title, media_url, sub_lang, sub_url, thumb_url):
        self.device = device
        self.callback(Renderer.STATE, "Searching for chromecasts...")

        self.cast = None
        # Use this event to signal the main thread that the requested chromecast was found.
        self.done_discovery = Event()

        self.stop_discovery = pychromecast.get_chromecasts(blocking=False, callback=self.chromecast_callback)
        # Wait at most 10 seconds for the device to be found.
        self.done_discovery.wait(10)
        # Polling/Blocking method.
        #chromecast = pychromecast.get_chromecasts()
        #cast = next(cc for cc in chromecast if cc.device.friendly_name == args.device)
        if self.cast:
            self.callback(Renderer.STATE, "Connecting to {}...".format(self.cast.device.friendly_name))

            self.rc = self.cast.socket_client.receiver_controller
            self.rc.register_status_listener(self)

            self.mc = self.cast.media_controller
            self.mc.register_status_listener(self)

            self.cast.register_handler(self.lc)

            # Get IP from chromecast status information.
            ip = self.cast.socket_client.socket.getsockname()[0]
            # Set metadata type to Movie type.
            metadata = {'metadataType': 1}

            # Start the Chromecast application.
            self.lc.launch_app();
            # Wait for media session to be ready.
            while ('urn:x-cast:com.google.cast.media' not in self.namespaces):
                polltime = 0.1
                can_read, _, _ = select.select([self.cast.socket_client.get_socket()], [], [], polltime)
                if can_read:
                    #received something on the socket, handle it with run_once()
                    self.cast.socket_client.run_once()

            # Start playback.
            self.mc._send_start_play_media(media_url, "video/mp4", title, thumb_url, metadata=metadata)
            if sub_url:
                self.load_subtitle_url(sub_url)
            # Set default subtitle style.
            self.set_subtitle_style()
        else:
            if self.device:
                self.callback(Renderer.STATE, "Oops, {} not found...".format(self.device))
            else:
                self.callback(Renderer.STATE, "No chromecasts found...")

    def handle(self):
        if not self.cast:
            return
        # Don't know what this does, but it is necessary for pychromecast to work.
        polltime = 0.1
        can_read, _, _ = select.select([self.cast.socket_client.get_socket()], [], [], polltime)
        if can_read:
            #received something on the socket, handle it with run_once()
            self.cast.socket_client.run_once()

    def pause(self, pause=None):
        if not self.cast:
            return
        # Pause/unpause function
        # Toggle pause state if the pause argument wasn't supplied.
        # Otherwise use the pause state to play/pause, even if it is already in that state.
        if self.mc.status.player_is_paused == True or pause == False:
            self.mc.play()
        elif pause == None or pause == True:
            self.mc.pause()

    def seek(self, seconds, position=0):
        if seconds:
            # Perform a relative seek.
            self.mc.seek(self.timeline.current_time + seconds)
        else:
            # Seek to a specific location of the media.
            self.mc.seek(position)

    def volume(self, volume, volume_abs=0):
        if volume:
            # Increase/decrease volume.
            if volume >= 0:
                self.cast.volume_up(volume)
            else:
                self.cast.volume_down(-volume)
        else:
            self.cast.set_volume(volume_abs)

    def stop(self):
        if not self.cast:
            return
        self.timeline.stop()
        # Stop media and quit app.
        try:
            self.mc.stop()
            self.cast.quit_app()
        except:
            # Media playback has already been stopped.
            # Or session is no longer active.
            pass
