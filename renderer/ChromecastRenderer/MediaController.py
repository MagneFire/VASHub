from pychromecast.controllers import BaseController
from pycaption import detect_format
import urllib.request

APP_MEDIA_RECEIVER = 'E7BE17DF'
MESSAGE_TYPE = 'type'

class SubEnum():
    """ Provide a basic class for const subtitle variables. """
    # Courtesy of https://stackoverflow.com/a/32796572
    @classmethod
    def has_value(cls, to_find):
        states = [val for key, val in vars(cls).items()
                  if not key.startswith("__")]
        return to_find in states


    @classmethod
    def get_keys(cls):
        states = [key for key in vars(cls)
                  if not key.startswith("__")]
        return states

    @classmethod
    def get_values(cls):
        states = [val for key, val in vars(cls).items()
                  if not key.startswith("__")]
        return states

class SubVertical(SubEnum):
    HORIZONTAL = ''
    LEFT = 'rl'
    RIGHT = 'lr'

class SubAlign(SubEnum):
    START = 'start'
    CENTER = 'middle'
    END = 'end'
    LEFT = 'left'
    RIGHT = 'right'

class SubLine(SubEnum):
    AUTO = 'auto'
    LINES = range(1, 24)

class SubBackground(SubEnum):
    TRANSPARANT = 'transparent'
    WHITE = 'white'
    BLACK = 'black'

class SubColor(SubEnum):
    WHITE = "white"
    YELLOW = "yellow"
    GREEN = "green"
    RED = "red"
    BLUE = "blue"
    BLACK = "black"

class LCController(BaseController):
    subtitle_size = 100
    subtitle_background = SubBackground.TRANSPARANT
    subtitle_color = SubColor.WHITE
    subtitle_position = SubLine.AUTO
    subtitle_align = SubAlign.CENTER
    subtitle_vertical = SubVertical.HORIZONTAL

    def __init__(self):
        super(LCController, self).__init__(
            "urn:x-cast:com.google.cast.sample.closecaption")

        self.media_session_id = 0
        self.app_id = APP_MEDIA_RECEIVER
        self._status_listeners = []

    def channel_connected(self):
        """ Called when media channel is connected. Will update status. """
        #self.update_status()
        return

    def channel_disconnected(self):
        """ Called when a media channel is disconnected. Will erase status. """
        #self.status = MediaStatus()
        #self._fire_status_changed()
        return

    def receive_message(self, message, data):
        """ Called when a media message is received. """
        return True

    def register_status_listener(self, listener):
        """ Register a listener for new media statusses. A new status will
            call listener.new_media_status(status) """
        self._status_listeners.append(listener)

    def update_status(self, callback_function_param=False):
        """ Send message to update the status. """
        self.send_message({MESSAGE_TYPE: TYPE_GET_STATUS},
                          callback_function=callback_function_param)

    def _send_command(self, command):
        """ Send a command to the Chromecast on media channel. """
        if self.media_session_id is None:
            self.logger.warning(
                "%s command requested but no session is active.",
                command[MESSAGE_TYPE])
            return

        command['mediaSessionId'] = self.media_session_id

        self.send_message(command, inc_session_id=True)

    def load_subtitle_file(self, sub_file):
        """ Load a subtitle from file. """
        sub = open(sub_file, 'rb').read()

        # Decode and send subtitle to chromecast.
        self.load_subtitle(sub)

    def load_subtitle_url(self, sub_url):
        """ Load a subtitle from a remote url. """
        sub = urllib.request.urlopen(sub_url).read()

        # Decode and send subtitle to chromecast.
        self.load_subtitle(sub)

    def load_subtitle(self, sub):
        """ Decode subtitle content(srt, webvtt, etc) and send that to the Chromecast """
        print("Loading subtitle...")
        # Assume that sub is properly decoded when it is of type str.
        if (not isinstance(sub, str)):
            # Attempt to decode the subtitle file using some codecs.
            try:
                sub = sub.decode('utf-8')
            except:
                try:
                    sub = sub.decode('cp1252')
                except:
                    # No valid text format detected, print error and return.
                    print("Failed to decode the subtitle file, incorrect codec detected!")
                    # Stop loading subtitle, but let playback continue.
                    return
                pass

        # Figure out what format the subtitle is. This returns a reader that is specific to the detected format.
        reader = detect_format(sub)
        # Feed all subtitle data to the detected reader.
        capset = reader().read(sub)
        # Get the list of decoded captions, 'en-US' is the default language used by the library.
        caplist = capset._captions['en-US']

        # Send subtitle to the Chromecast.
        self.send_subtitle(caplist)

    def send_subtitle(self, subtitle):
        """ Send subtitle to the Chromecast """
        # Clear the current subtitles used on the Chromecast.
        self._send_command({MESSAGE_TYPE: 'ENABLE_CC', 'caption':''})
        for cap in subtitle:
            # Get time in seconds.
            start = cap.start / 1000000.0
            end = cap.end / 1000000.0
            text = cap.get_text()
            # Encode the subtitle to the Chromecast compatible format.
            self._send_command({MESSAGE_TYPE: 'ENABLE_CC_ADDMORE_NEW', 'caption': text + '<;;>' + str(start) + '<;;>' + str(end) + '<;Y;>'})
        # Show the subtitle track.
        self._send_command({MESSAGE_TYPE: 'ENABLE_CC_DONE_NEW'})

        return

    def hide_subtitle(self):
        # Use a workaround to hide the subtitle.
        # It is not possible to hide the subtitle directly.
        # The workaround is to first show the subtitle, and then toggle the visibility.
        self.show_subtitle()
        # Now hide the subtitle, using the toggle function.
        self._send_command({MESSAGE_TYPE: 'TOGGLE_SUBTITLE'})

    def show_subtitle(self):
        # The 'ENABLE_CC_DONE_NEW' message type shows the subtitle.
        # Normally this option is used to indicate the app that we are done sending a subtitle.
        self._send_command({MESSAGE_TYPE: 'ENABLE_CC_DONE_NEW'})

    def toggle_subtitle(self):
        """ Toggle subtitle on/off, only works when a subtitle is loaded """
        self._send_command({MESSAGE_TYPE: 'TOGGLE_SUBTITLE'})

    def set_subtitle_style(self, size=None, background=None, color=None, position=None, align=None, vertical=None):
        """ Change the current subtitle style used. """
        # 'size' has a range from 0% to 1000%.
        if size and size > 0 and size <= 1000:
            self.subtitle_size = size
        if background and SubBackground.has_value(background):
            self.subtitle_background = background
        if color and SubColor.has_value(color):
            self.subtitle_color = color
        if position:
            if position == SubLine.AUTO:
                self.subtitle_position = position
            elif position in SubLine.LINES:
                self.subtitle_position = str(position)
        if align and SubAlign.has_value(align):
            self.subtitle_align = align
        if vertical and SubVertical.has_value(vertical):
            self.subtitle_vertical = vertical

        msg = {MESSAGE_TYPE: 'SUBTITLE_SIZE_AND_BACKGROUND_AND_COLOR_AND_POSITION',
                'size': str(self.subtitle_size) + '%',
                'background': self.subtitle_background,
                'color': self.subtitle_color,
                'position': self.subtitle_position,
                'align': self.subtitle_align,
                'vertical': self.subtitle_vertical}
        self._send_command(msg)

    def subtitle_offset(self, seconds):
        # Possible values: -0.1, -1.0, 1.0, 0.1
        self._send_command({MESSAGE_TYPE: 'SUBTITLE_CHANGE', 'timeSeconds': seconds})

    def subtitle_offset_reset(self):
        self._send_command({MESSAGE_TYPE: 'SUBTITLE_CHANGE_RESET'})

    def image_rotate_left(self):
        # Rotates the video left by 90 degrees.
        self._send_command({MESSAGE_TYPE: 'ROTATE_LEFT'})

    def image_rotate_right(self):
        # Rotates the video right by 90 degrees.
        self._send_command({MESSAGE_TYPE: 'ROTATE_RIGHT'})

    def image_zoom_in(self):
        self._send_command({MESSAGE_TYPE: 'ZOOM_IN'})

    def image_zoom_out(self):
        self._send_command({MESSAGE_TYPE: 'ZOOM_OUT'})

    def set_speed(self, speed):
        # Use double here to control the playback speed, 1.0 is normal playback speed.
        self._send_command({MESSAGE_TYPE: 'SPEED', 'speed': speed})

    def launch_app(self):
        receiver_ctrl = self._socket_client.receiver_controller
        receiver_ctrl.launch_app(self.app_id)

    def tear_down(self):
        """ Called when controller is destroyed. """
        super(LCController, self).tear_down()

        self._status_listeners[:] = []
