# Interface file
class Renderer():
    DURATION = 1
    TIME = 2
    QUIT = 3
    STATE = 4
    VOLUME = 5
    VOLUME_MUTE = 6
    def __init__(self, callback):
        self.callback = callback
        # Init
        return

    def play(self, device, title, media_url, sub_lang, sub_url, thumb_url):
        return

    def handle(self):
        return

    def pause(self):
        return

    def seek(self, seconds, position=0):
        """ Use `seconds` for relative seek. """
        """ Use `seconds`=0 and `position` to seek to a specific position. """
        return

    def volume(self, volume, volume_abs=0):
        return

    def stop(self):
        return
