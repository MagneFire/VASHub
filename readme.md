# VASHub
Stream media files to a Chromecast(or other renderer)!

### Features
- A simple server for serving media files.
- Chromecast renderer support.
  - Seek support
  - Subtitle support
- Local renderer support.

### Project structure

    .
    ├── LICENSE
    ├── readme.md
    ├── renderer
    │   ├── ChromecastRenderer
    │   │   ├── ChromecastRenderer.py
    │   │   ├── MediaController.py
    │   ├── LocalRenderer
    │   │   ├── LocalRenderer.py
    │   │   ├── MediaPlayer
    │   │   │   ├── MediaPlayer.py
    │   │   │   ├── MPVMediaPlayer.py
    │   │   │   └── VLCMediaPlayer.py
    │   └── Renderer.py
    ├── server.py
    ├── ui
    │   ├── cli.py
    │   ├── ui.py
    │   └── web.py
    └── vashub.py

### How to use
There are several ways to use this project. Use the `vashub.py --help` command to get a list of known commands. This can also be used to figure out which renderers are supported on your system.

```
usage: vashub.py [-h] [--media-type MEDIA_TYPE] [--media-file MEDIA_FILE]
                 [--media-port MEDIA_PORT] [--sub-file SUB_FILE]
                 [--sub-lang SUB_LANG] [--sub-port SUB_PORT]
                 [--thumb-file THUMB_FILE] [--thumb-port THUMB_PORT]
                 [--title TITLE] [--season SEASON] [--episode EPISODE]
                 [--store] [--no-ui] [--enable-log] [--device DEVICE]
                 [--renderer RENDERER]
                 path

Stream video/audio.

positional arguments:
  path                  Location to .mediaInfo file/folder or media file.

optional arguments:
  -h, --help            show this help message and exit
  --media-type MEDIA_TYPE
                        Specify the type of media that is beign served. Use
                        one of the following media types:
                        ['MEDIA_TYPE_TVSHOW', 'MEDIA_TYPE_PHOTO',
                        'MEDIA_TYPE_MUSIC', 'MEDIA_TYPE_GENERIC',
                        'MEDIA_TYPE_MOVIE']
  --media-file MEDIA_FILE
                        Override the media file provided by the media
                        information file.
  --media-port MEDIA_PORT
                        Use a specific port to serve media file on. By default
                        this is determined by the operating system.
  --sub-file SUB_FILE   Use a subtitle with the current media.
  --sub-lang SUB_LANG   Set a subtitle language associated with the subtitle
                        file.
  --sub-port SUB_PORT   Use a specific port to serve subtitle file on. By
                        default this is determined by the operating system.
  --thumb-file THUMB_FILE
                        Serve a thumbnail with the current media.
  --thumb-port THUMB_PORT
                        Use a specific port to serve thumbnail file on. By
                        default this is determined by the operating system.
  --title TITLE         Set the title of the current media.
  --season SEASON       Set the season of the currently playing serie. Only
                        used when playing series.
  --episode EPISODE     Set the episode of the currently playing serie. Only
                        used when playing series.
  --store               Store defined arguments to a media info file.
  --no-ui               Disable command line interface.
  --enable-log          Enable logger in the user interface
  --device DEVICE       Specify the device name of the chromecast. By default
                        the first detected will be used for playback. Only
                        used with the chromecast renderer.
  --renderer RENDERER   Specify a renderer for playback of the served media.
                        Use one of the following renderers: ['Chromecast',
                        'None']

```

### Create a media information file.
This program can generate media information files (.mediaInfo) to allow for easier playback.
The general way to do this is to use the `--store` command to store the other commands into a media information file.
Example command:
```
vashub.py --media-type <media type> --sub-file <sub-file> --sub-lang <language> --thumb-file <thumb-file> --title <Media title> --store [Desired path to store .mediaInfo file]
```

#### Stream to Chromecast
The default renderer used is the Chromecast renderer.
To play a media information file use the following command:
```
vashub.py --device <Chromecast name> <dir to media info file>.mediaInfo
```
To play a media file
```
vashub.py --device <Chromecast name> --sub-file <subfile VTT> --thumb-file <thumb file> <path to media file>
```

### xxx renderer is not available
This program determines at runtime which renderers are available. It does this based on the needed libraries installed for the specific renderer. The renderer will by disabled when a necessary library was not found.
In order for the Chromecast renderer to work make sure that the following libraries are installed:
```bash
pip install pychromcast
pip install pycaption
```
In order for the local renderer to work:
```bash
pip install python-mpv
```

### Transcoding
The current software does not have support for transcoding. However it is possible to use the piping feature in combination with `FFmpeg` to transcode media. Use the following command to transcode a media file to a Chromecast compatible video format:
```
ffmpeg -i <media file> -preset ultrafast -f mp4 -movflags frag_keyframe+faststart pipe:1 | vashub.py --device <device name> -
```

### License
This project uses the BSD license, more information about this can be found in the `LICENSE` file on the repository. Other parts of the application are licensed under their respective licenses (which are included in the source directories of those parts).
