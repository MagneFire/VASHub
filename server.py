# Server file
# Provide servers for subtitle, audio, video

# This code includes a modified version of RangeHTTPServer

import threading
import time
import os, errno, sys
from http.server import SimpleHTTPRequestHandler, BaseHTTPRequestHandler, HTTPServer
import re
import socket
import posixpath
from urllib.parse import *
from http import HTTPStatus

def copy_byte_range(infile, outfile, start=None, stop=None, bufsize=16*1024):
    '''Like shutil.copyfileobj, but only copy a range of the streams.
    Both start and stop are inclusive.
    '''
    if start is not None: infile.seek(start)
    while 1:
        to_read = min(bufsize, stop + 1 - infile.tell() if stop else bufsize)
        buf = infile.read(to_read)
        if not buf:
            break
        try:
            # Write data to client, using a try-catch method.
            # This is needed for seeking. When seeking is involved
            # the connection might be abruptly broken.
            outfile.write(buf)
        except:
            # No problem, probably seeking.
            return;


BYTE_RANGE_RE = re.compile(r'bytes=(\d+)-(\d+)?$')


def parse_byte_range(byte_range):
    '''Returns the two numbers in 'bytes=123-456' or throws ValueError.
    The last number or both numbers may be None.
    '''
    if byte_range.strip() == '':
        return None, None

    m = BYTE_RANGE_RE.match(byte_range)
    if not m:
        raise ValueError('Invalid byte range %s' % byte_range)

    first, last = [x and int(x) for x in m.groups()]
    if last and last < first:
        raise ValueError('Invalid byte range %s' % byte_range)
    return first, last


class MediaServer():
    def __init__(self):
        thumb_http = None
        media_http = None
        sub_http = None

    class MediaHandler(SimpleHTTPRequestHandler):
        content_type = None
        range_support = True

        def log_message(self, format, *args):
            return

        def guess_type(self, path):
            if self.content_type:
                return self.content_type
            else:
                return SimpleHTTPRequestHandler.guess_type(self, path)

        """Adds support for HTTP 'Range' requests to SimplerHTTPRequestHandler
        The approach is to:
        - Override send_head to look for 'Range' and respond appropriately.
        - Override copyfile to only transmit a range when requested.
        """
        def send_head(self):
            # Replace + signs with spaces in path.
            self.path = unquote_plus(self.path)

            # Mirroring SimpleHTTPServer.py here
            path = self.translate_path(self.path)
            f = None
            ctype = self.guess_type(path)
            self.range = None

            if os.path.isdir(path):
                parts = urlsplit(self.path)
                if not parts.path.endswith('/'):
                    # redirect browser - doing basically what apache does
                    self.send_response(HTTPStatus.MOVED_PERMANENTLY)
                    new_parts = (parts[0], parts[1], parts[2] + '/',
                                parts[3], parts[4])
                    new_url = urlunsplit(new_parts)
                    self.send_header("Location", new_url)
                    self.end_headers()
                    return None
                for index in "index.html", "index.htm":
                    index = os.path.join(path, index)
                    if os.path.exists(index):
                        path = index
                        break
                else:
                    return self.list_directory(path)

            ctype = self.guess_type(path)
            self.range = None
            try:
                f = None
                # Use the data piped to the programs input for the data stream.
                if self.path == "/-":
                    f = sys.stdin.buffer
                else:
                    f = open(path, 'rb')
            except IOError:
                self.send_error(HTTPStatus.NOT_FOUND, 'File not found')
                return None

            if 'Range' not in self.headers or not self.range_support:
                self.protocol_version = "HTTP/1.1"
                self.send_response(HTTPStatus.OK)
                self.send_header("Content-type", ctype)
                self.send_header('Access-Control-Allow-Origin', '*')
                self.end_headers()
                return f

            try:
                self.range = parse_byte_range(self.headers['Range'])
            except ValueError as e:
                self.send_error(HTTPStatus.BAD_REQUEST, 'Invalid byte range')
                return None
            first, last = self.range

            fs = os.fstat(f.fileno())
            file_len = fs[6]
            if first >= file_len:
                self.send_error(HTTPStatus.REQUEST_RANGE_NOT_SATISFIABLE, 'Requested Range Not Satisfiable')
                return None

            self.send_response(HTTPStatus.PARTIAL_CONTENT)
            # Mandatory! for use with subtitles!
            self.send_header('Access-Control-Allow-Origin', '*')
            self.send_header('Content-type', ctype)
            self.send_header('Accept-Ranges', 'bytes')

            if last is None or last >= file_len:
                last = file_len - 1
            response_length = last - first + 1

            self.send_header('Content-Range',
                             'bytes %s-%s/%s' % (first, last, file_len))
            self.send_header('Content-Length', str(response_length))
            self.send_header('Last-Modified', self.date_time_string(fs.st_mtime))
            self.end_headers()
            return f

        def copyfile(self, source, outputfile):
            if not self.range:
                return SimpleHTTPRequestHandler.copyfile(self, source, outputfile)

            # SimplerHTTPRequestHandler uses shutil.copyfileobj, which doesn't let
            # you stop the copying before the end of the file.
            start, stop = self.range  # set in send_head()
            copy_byte_range(source, outputfile, start, stop)

        def set_range_support(self, support):
            # Enable/disable support for the client to get a specific part of the data.
            self.range_support = support
            return

    class SubMediaHandler(MediaHandler):
        content_type = "text/vtt;charset=utf-8"

    def set_range_support(self, support):
        self.media_http.RequestHandlerClass.set_range_support(self.MediaHandler, support)
        self.sub_http.RequestHandlerClass.set_range_support(self.MediaHandler, support)
        self.thumb_http.RequestHandlerClass.set_range_support(self.MediaHandler, support)
        return

    def startserver(self, media_dir, media_port=0, sub_port=0, thumb_port=0):
        # All files will be located relative to the media directory.
        os.chdir(media_dir)
        media_address = ("", media_port)
        sub_address = ("", sub_port)
        thumb_address = ("", thumb_port)

        self.media_http = HTTPServer(media_address, self.MediaHandler)
        self.sub_http = HTTPServer(sub_address, self.SubMediaHandler)
        self.thumb_http = HTTPServer(thumb_address, self.MediaHandler)

        media_port = self.media_http.socket.getsockname()[1]
        thumb_port = self.thumb_http.socket.getsockname()[1]
        sub_port = self.sub_http.socket.getsockname()[1]

        thread = threading.Thread(target=self.media_http.serve_forever)
        thread.daemon = True
        thread.start()
        print("Started media server on port {}".format(media_port))
        thread = threading.Thread(target=self.sub_http.serve_forever)
        thread.daemon = True
        thread.start()
        print("Started sub server on port {}".format(sub_port))
        thread = threading.Thread(target=self.thumb_http.serve_forever)
        thread.daemon = True
        thread.start()
        print("Started thumb server on port {}".format(thumb_port))

        return media_port, sub_port, thumb_port

    def stopserver(self):
        sys.stdout.write("Closing servers ...")
        sys.stdout.flush()
        self.thumb_http.shutdown()
        sys.stdout.write(" Thumb")
        sys.stdout.flush()
        self.sub_http.shutdown()
        sys.stdout.write(" Sub")
        sys.stdout.flush()
        self.media_http.shutdown()
        sys.stdout.write(" Media")
        sys.stdout.write("\r\n")
        sys.stdout.flush()

if __name__ == "__main__":
    ms = MediaServer()
    ms.startserver(".")
