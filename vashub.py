#!/usr/bin/env python3
import sys, os, errno
import signal
import argparse
import json
import time
import socket
from collections import OrderedDict
import socket

from server import MediaServer
try:
    from renderer.ChromecastRenderer.ChromecastRenderer import ChromecastRenderer
except:
    pass
try:
    from renderer.LocalRenderer.LocalRenderer import LocalRenderer
except:
    pass
from renderer.Renderer import Renderer
from ui.cli import CommandLineInterface
from ui.ui import UI

def get_ip():
    # Connect to Google to get the IP address of this system.
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    return ip

class VasHub():
    media_types = {'GENERIC': 0,
                'MOVIE': 1,
                'TVSHOW': 2,
                'MUSIC': 3,
                'PHOTO': 4}

    renderers = OrderedDict()

    def cli_callback(self, action):
        if action == UI.QUIT:
            self.stop = True
        elif action == UI.STOP:
            self.stop = True
        elif action == UI.SEEK_LEFT_5S:
            self.renderer.seek(-5)
        elif action == UI.SEEK_LEFT_1M:
            self.renderer.seek(-60)
        elif action == UI.SEEK_LEFT_10M:
            self.renderer.seek(-600)
        elif action == UI.SEEK_RIGHT_5S:
            self.renderer.seek(5)
        elif action == UI.SEEK_RIGHT_1M:
            self.renderer.seek(60)
        elif action == UI.SEEK_RIGHT_10M:
            self.renderer.seek(600)
        elif action == UI.VOLUME_UP:
            self.renderer.volume(0.1)
        elif action == UI.VOLUME_DOWN:
            self.renderer.volume(-0.1)
        elif action == UI.PAUSE:
            self.renderer.pause()
        elif action == UI.REWIND:
            return
        elif action == UI.CYCLE_UP_SUB:
            self.renderer.toggle_subtitle();
            return
        elif action == UI.CYCLE_DOWN_SUB:
            self.renderer.toggle_subtitle();
            return
        elif action == UI.BACK:
            return
        elif action == UI.FORWARD:
            return

    def renderer_callback(self, key, value):
        if key == Renderer.STATE:
            self.cli.set_state(value)
        if key == Renderer.DURATION:
            self.cli.set_duration(value)
        if key == Renderer.TIME:
            self.cli.set_time(value)
        if key == Renderer.QUIT:
            self.stop = True

    def populate_renderers(self):
        # Figure out which renderers exist based on the modules installed.
        if 'ChromecastRenderer' in globals():
            self.renderers['Chromecast'] = ChromecastRenderer
        if 'LocalRenderer' in globals():
            self.renderers['Local'] = LocalRenderer
        if 'Renderer' in globals():
            self.renderers['None'] = Renderer

    def verify_paths(self, base_path, paths):
        # This function checks if the givens paths are a sub path ot the given base path.
        # It also verifies if the specified path exists.
        valid = True
        for path in paths:
            # Ignore path when using pipe/stdin.
            if path == '-':
                continue
            file = base_path + os.sep + path
            absfile = os.path.abspath(file)
            if not os.path.isfile(file):
                valid = False
                print("WARNING: {} does not exist in {}".format(path, base_path + os.sep))
            if not absfile.startswith(base_path + os.sep):
                valid = False
                print("WARNING: {} is not in a sub directory of {}".format(path, base_path + os.sep))
        return valid

    def __init__(self):
        self.stop = False
        self.renderer = None
        self.cli = None
        self.mediaserver = None
        self.media_type = 0
        sub_lang = None
        media_url = None
        sub_url = None
        thumb_url = None
        # Get all available renderers.
        self.populate_renderers()

        # Define all command line arguments.
        parser = argparse.ArgumentParser(description='Stream video/audio.')
        parser.add_argument('--media-type', help="Specify the type of media that is being served. Use one of the following media types: {}". format([item[0] for item in self.media_types.items()]))
        parser.add_argument('--media-file', help="Override the media file provided by the media information file.")
        parser.add_argument('--media-port', type=int, help="Use a specific port to serve media file on. By default this is determined by the operating system.")
        parser.add_argument('--sub-file', action='append', help="Use a subtitle with the current media.")
        parser.add_argument('--sub-lang', action='append', help="Set a subtitle language associated with the subtitle file.")
        parser.add_argument('--sub-port', type=int, help="Use a specific port to serve subtitle file on. By default this is determined by the operating system.")
        parser.add_argument('--thumb-file', help="Serve a thumbnail with the current media.")
        parser.add_argument('--thumb-port', type=int, help="Use a specific port to serve thumbnail file on. By default this is determined by the operating system.")
        parser.add_argument('--title', help="Set the title of the current media.")
        parser.add_argument('--season', help="Set the season of the currently playing series. Only used when playing series.")
        parser.add_argument('--episode', type=int, help="Set the episode of the currently playing series. Only used when playing series.")
        parser.add_argument('--store', action='store_true', default=False, help="Store defined arguments to a media info file.")
        parser.add_argument('--disable-cli', action='store_true', default=False, help="Disable command line user interface.")
        parser.add_argument('--host', help="Override the host ip used. This is used to tell a renderer where the media server is located.")
        parser.add_argument('--cli-debug', action='store_true', default=False, help="Enable debug messages in the user interface.")
        parser.add_argument('--device', help="Specify the device name of the Chromecast. By default the first detected will be used for playback. Only used with the Chromecast renderer.")
        parser.add_argument('--renderer', help="Specify a renderer for playback of the served media. Use one of the following renderers: {}". format([item[0] for item in self.renderers.items()]))
        parser.add_argument('path', help="Location to .mediaInfo file/folder or media file.")

        args = parser.parse_args()
        path = args.path
        # Set the default subtitle language to English.
        if not args.sub_lang:
            args.sub_lang = ["en-US"]

        if args.media_type:
            self.media_type = self.media_types[args.media_type.upper()]

        if args.renderer and (args.renderer.title() not in self.renderers):
            print('Unknown renderer specified: {}'.format(args.renderer))
            print('Valid renderers are: {}'.format(format([item[0] for item in self.renderers.items()])))
            sys.exit()

        # It is currently not possible to use the piped input and command line interface together.
        # Because there is only one stdin file descriptor available.
        if path == '-':
            args.disable_cli = True

        self.cli = CommandLineInterface(self.cli_callback, args.disable_cli, args.cli_debug)

        old_stdout = sys.stdout
        sys.stdout = self.cli.redirect(old_stdout)
        print("Welcome to VASHub!")

        if os.path.isdir(path):
            path_dir = os.path.abspath(args.path)
        else:
            path_dir = os.path.dirname(os.path.abspath(args.path))

        if os.path.isdir(path):
            if args.store:
                # File already exists!
                self.create_mediafile(path_dir, args)
                self.shutdown()
            # Search for .mediaInfo.
        elif os.path.isfile(path) or path == '-':
            # Create a dummy mediaInfo.
            mediaInfo = {'media': '',
                         'title': '',
                         'subtitles': []}

            if args.store:
                # File already exists!
                self.create_mediafile(path_dir, args)
                self.shutdown()
            # When filename is .mediaInfo, use this file otherwise play mediafile
            if os.path.splitext(path)[1] == '.mediaInfo' or os.path.basename(path) == '.mediaInfo':
                print("Using mediaInfo file.")
                mediaInfo = json.load(open(path))
            else:
                print("Playing local file.")
                mediaInfo['media'] = os.path.basename(args.path)

            print("Starting media server...")

            if args.media_file:
                mediaInfo['media'] = args.media_file
            if args.thumb_file:
                mediaInfo['thumb'] = args.thumb_file
            if args.sub_file:
                if len(args.sub_file) != len(args.sub_lang):
                    print("Warning: Not the same amount of subtitle files and subtitle languages supplied!")
                    # Repeat sub languages when not enough supplied.
                    if len(args.sub_lang) < len(args.sub_file):
                        for i in range(len(args.sub_lang), len(args.sub_file)):
                            # Repeat the last entry of the sub_lang list.
                            args.sub_lang.append(args.sub_lang[-1])
                # Add all command line subtitles.
                for i in range(0, len(args.sub_file)):
                    mediaInfo['subtitles'].append({args.sub_lang[i]: args.sub_file[i]})

            # Verify that the defined files actually exist.
            paths = []
            paths.append(mediaInfo['media'])
            if 'thumb' in mediaInfo:
                paths.append(mediaInfo['thumb'])
            for sub in mediaInfo['subtitles']:
                paths.append(next(iter(sub.values())))

            if not self.verify_paths(path_dir, paths):
                print("ERROR: file paths are not valid!")
                print("Make sure that the supplied files exist and that they are in a subdirectory of the path: {}".format(path_dir + os.sep))
                self.shutdown()

            # Let OS decide what open ports to use by default.
            port = 0
            thumb_port = 0
            sub_port = 0
            # Allow user to override these.
            if args.media_port:
                port = args.media_port
            if args.thumb_port:
                thumb_port = args.thumb_port
            if args.sub_port:
                sub_port = args.sub_port

            self.mediaserver = MediaServer()
            port, sub_port, thumb_port = self.mediaserver.startserver(path_dir, port, sub_port, thumb_port)
            if path == '-':
                # The piped data does not support seeking.
                # Disabling range requests disables seeking support.
                self.mediaserver.set_range_support(False)

            if args.host:
                # Manually override the ip.
                ip = args.host
            else:
                # Get the host IP by connecting.
                ip = get_ip()

            # Get the first subtitle listed, and get the first element of that object
            if mediaInfo['subtitles']:
                sub_lang = list(mediaInfo['subtitles'][0])[0]

            # Check if defined renderer is a valid renderer
            if args.renderer and (args.renderer.title() in self.renderers):
                # Get renderer from user selected list
                self.renderer = self.renderers[args.renderer.title()](self.renderer_callback)
            else:
                # Get first renderer in list.
                self.renderer = next(iter(self.renderers.items()))[1](self.renderer_callback)

            # Set all urls.
            media_url = "http://{}:{}/{}".format(ip, port, mediaInfo['media'])
            if sub_lang:
                sub_url = "http://{}:{}/{}".format(ip, sub_port, mediaInfo['subtitles'][0][sub_lang])
            if 'thumb' in mediaInfo:
                thumb_url = "http://{}:{}/{}".format(ip, thumb_port, mediaInfo['thumb'])

            print("Media url: {}".format(media_url))
            print("Thumbnail url: {}".format(thumb_url))
            print("Subtitle url: {}".format(sub_url))

            # Play media in renderer.
            self.renderer.play(args.device, mediaInfo['title'], media_url, sub_lang, sub_url, thumb_url)

            try:
                while not self.stop:
                    self.cli.handle()
                    self.renderer.handle()
                    # Don't eat up to much CPU time.
                    time.sleep(0.01)
            except KeyboardInterrupt:
                pass
            except:
                pass
        elif args.store:
            # Store information and create a new media info file.
            self.create_mediafile(path_dir, args)
        else:
            print("Error invalid path defined!")

        self.shutdown()

    def shutdown(self):
        if self.cli:
            self.cli.set_state("STOPPING")
            self.cli.handle()
        if self.renderer:
            self.renderer.stop()
        if self.mediaserver:
            self.mediaserver.stopserver()
        if self.cli:
            self.cli.stop()
        sys.exit()
        

    def create_mediafile(self, path, args):
        print("Creating media file...")
        mediaInfo = {}
        mediaInfo['title'] = args.title
        mediaInfo['media-type'] = self.media_type
        thumb = ""
        if args.thumb_file:
            thumb = os.path.relpath(args.thumb_file, path)
            mediaInfo['thumb'] = thumb
            media = ""
        if args.media_file:
            media = os.path.relpath(args.media_file, path)
            mediaInfo['media'] = media
        mediaInfo['subtitles'] = []
        sub = ""
        if args.sub_file:
            for i in range(0, len(args.sub_file)):
                sub = os.path.relpath(args.sub_file[i], path)
                mediaInfo['subtitles'].append({args.sub_lang[i]: sub})

        # TV Show specific variables.
        if args.season:
            mediaInfo['season'] = args.season
        if args.episode:
            mediaInfo['episode'] = args.episode

        # Music specific variables
        # Photo specific variables

        # Write new mediaInfo to a new file.
        with open(path+'/.mediaInfo', 'w') as jsonfile:
            json.dump(mediaInfo, jsonfile, indent=4, ensure_ascii=False)

if __name__ == "__main__":
    VasHub()

