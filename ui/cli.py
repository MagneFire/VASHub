#!/usr/bin/env python3
import os, time, errno, sys
import curses
from curses.textpad import Textbox, rectangle
import textwrap
import datetime
from .ui import UI

# Set the keys that will be used for the cli.
CLI_PAUSE = ord(" ")
CLI_SUB_UP = ord("j")
CLI_SUB_DOWN = ord("J")
CLI_STOP = ord("s")
CLI_REWIND = curses.KEY_BACKSPACE
CLI_BACK = ord("<")
CLI_FORWARD_0 = ord(">")
CLI_FORWARD_1 = 10 # Enter key
CLI_VOLUME_DOWN = ord("9")
CLI_VOLUME_UP = ord("0")
CLI_QUIT = ord("q")
CLI_SEEK_LEFT_5S = curses.KEY_LEFT
CLI_SEEK_LEFT_1M = curses.KEY_DOWN
CLI_SEEK_LEFT_10M = curses.KEY_NPAGE
CLI_SEEK_RIGHT_5S = curses.KEY_RIGHT
CLI_SEEK_RIGHT_1M = curses.KEY_UP
CLI_SEEK_RIGHT_10M = curses.KEY_PPAGE

# Define the position of the different components
CLI_STATE = [0, 1]
CLI_TIMELINE = [1, 2]

class CommandLineInterface(UI):
    class Logger():
        # The logger overrides the stdout provided by Python
        # This puts the debug output in a dedicated window.
        def __init__(self, cli):
            self.cli = cli

        def redirect(self, stdout):
            self.old_stdout = stdout

        def write(self, text):
            if self.cli.logwindow:
                self.cli.logwindow.addstr(text)
                self.cli.screen.refresh()
                self.cli.logwindow.refresh()
            return

        def flush(self):
            # Do nothing
            #self.old_stdout.flush()
            return

    def redirect(self, stdout):
        if self.verbose:
            return stdout
        self.logger.redirect(stdout)
        return self.logger

    def __init__(self, callback, verbose=False, logWindow=False):
        self.verbose = verbose
        self.logwindow = None
        if not self.verbose:
            self.screen = curses.initscr()
            curses.start_color()
            curses.use_default_colors()
            # Fill color pallete with data.
            for i in range(0, curses.COLORS):
                curses.init_pair(i + 1, -1, i)

            self.logger = self.Logger(self)
            self.callback = callback
            self.height, self.width = self.screen.getmaxyx()
            self.screen.nodelay(1)
            curses.noecho()
            curses.curs_set(0)
            self.screen.keypad(1)
            curses.mousemask(1)
            if logWindow:
                # Create a window for log data at with size and at offset.
                self.logwindow = curses.newwin(int(self.height/2), self.width, int(self.height/2), 0)
                self.logwindow.scrollok(1)

        self.media_duration = 1
        self.media_time = 0
        self.state = "IDLE"

    def set_state(self, state):
        print("State: {}".format(state))
        self.state = state
        return

    def draw_mediaplayer(self):
        # Draw all available colors.
        #self.screen.addstr(0, 0, "", curses.color_pair(0))
        #for i in range(0, 255):
        #    self.screen.addstr(str(i), curses.color_pair(i))

        self.screen.move(CLI_STATE[0], 0)
        self.screen.clrtoeol() # Remove remaining characters on the state line.
        self.screen.addstr(CLI_STATE[0], 0, "State: {}".format(self.state), curses.color_pair(0))
        if self.media_time <= self.media_duration:
            bar_ypos = CLI_TIMELINE[0]
            media_time = datetime.timedelta(seconds=self.media_time)
            self.screen.addstr(bar_ypos, 0, str(media_time), curses.color_pair(0))
            media_left = datetime.timedelta(seconds=(self.media_duration - self.media_time))
            self.screen.addstr(bar_ypos, self.width - 7, str(media_left), curses.color_pair(0))

            bar_ypos += 1
            bar_position = int((self.media_time/self.media_duration) * self.width)

            if bar_position >= 0:
                for x in range(0, bar_position):
                    self.screen.addstr(bar_ypos, x, ' ', curses.color_pair(5))
                for x in range(bar_position, self.width):
                    self.screen.addstr(bar_ypos, x, ' ', curses.color_pair(8))
        return

    def handle(self):
        if not self.verbose:
            # Get non-blocking character.
            event = self.screen.getch()

            if event == CLI_QUIT:
                self.callback(UI.QUIT)
            elif event == CLI_PAUSE:
                self.callback(UI.PAUSE)
            elif event == CLI_SUB_UP:
                self.callback(UI.CYCLE_UP_SUB)
            elif event == CLI_SUB_DOWN:
                self.callback(UI.CYCLE_UP_DOWN)
            elif event == CLI_STOP:
                self.callback(UI.STOP)
            elif event == CLI_REWIND:
                self.callback(UI.REWIND)
            elif event == CLI_BACK:
                self.callback(UI.BACK)
            elif (event == CLI_FORWARD_0) or (event == CLI_FORWARD_1):
                self.callback(UI.FORWARD)
            elif event == CLI_VOLUME_UP:
                self.callback(UI.VOLUME_UP)
            elif event == CLI_VOLUME_DOWN:
                self.callback(UI.VOLUME_DOWN)
            elif event == CLI_SEEK_LEFT_5S:
                self.callback(UI.SEEK_LEFT_5S)
            elif event == CLI_SEEK_LEFT_1M:
                self.callback(UI.SEEK_LEFT_1M)
            elif event == CLI_SEEK_LEFT_10M:
                self.callback(UI.SEEK_LEFT_10M)
            elif event == CLI_SEEK_RIGHT_5S:
                self.callback(UI.SEEK_RIGHT_5S)
            elif event == CLI_SEEK_RIGHT_1M:
                self.callback(UI.SEEK_RIGHT_1M)
            elif event == CLI_SEEK_RIGHT_10M:
                self.callback(UI.SEEK_RIGHT_10M)

            elif event == curses.KEY_MOUSE:
                try:
                    _, col, row, _, _ = curses.getmouse()
                    #self.screen.addstr(row, col,"Click: col:{} row:{}\n\n".format(row, col))
                    self.callback(UI.PAUSE)
                except:
                    pass
            elif event == curses.KEY_RESIZE:
                self.screen.redrawwin()
                if self.logwindow:
                    self.logwindow.redrawwin()
                self.height, self.width = self.screen.getmaxyx()
                self.screen.clear()
                #self.screen.addstr(0, 0, "Screen size: {} {}\n".format(self.height, self.width))

            self.draw_mediaplayer()
            # Update all data.
            self.screen.refresh()
            if self.logwindow:
                self.logwindow.refresh()

    def stop(self):
        if not self.verbose:
            curses.endwin()

    def write(self, string):
        # Reserver for media player
        #self.screen.addstr(string)
        return

    def set_duration(self, duration):
        self.media_duration = duration

    def set_time(self, time):
        self.media_time = time

if __name__ == "__main__":
    stop = False

    def i_callback(action):
        global stop
        if action == CommandLineInterface.QUIT:
            stop = True
        elif action == CommandLineInterface.STOP:
            stop = True
        elif action == CommandLineInterface.SEEK_LEFT_5S:
            return
        elif action == CommandLineInterface.SEEK_LEFT_1M:
            return
        elif action == CommandLineInterface.SEEK_RIGHT_5S:
            return
        elif action == CommandLineInterface.SEEK_RIGHT_1M:
            return
        elif action == CommandLineInterface.VOLUME_UP:
            return
        elif action == CommandLineInterface.VOLUME_DOWN:
            return
        elif action == CommandLineInterface.PAUSE:
            return
        elif action == CommandLineInterface.REWIND:
            return
        elif action == CommandLineInterface.CYCLE_UP_SUB:
            return
        elif action == CommandLineInterface.CYCLE_DOWN_SUB:
            return
        elif action == CommandLineInterface.BACK:
            return
        elif action == CommandLineInterface.FORWARD:
            return

    cli = CommandLineInterface(i_callback)
    while not stop:
        cli.handle()
    cli.stop()
