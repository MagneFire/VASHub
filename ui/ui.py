class UI():
    QUIT = 2
    VOLUME_UP = 3
    VOLUME_DOWN = 4
    VOLUME_CHANGED = 19
    PAUSE = 5
    STOP = 6
    REWIND = 7 # Rewind to beginning
    CYCLE_UP_SUB = 8
    CYCLE_DOWN_SUB = 9
    BACK = 10
    FORWARD = 11
    SEEK_LEFT_5S = 12
    SEEK_LEFT_1M = 13
    SEEK_LEFT_10M = 16
    SEEK_RIGHT_5S = 14
    SEEK_RIGHT_1M = 15
    SEEK_RIGHT_10M = 17
    SEEK_POS = 18
